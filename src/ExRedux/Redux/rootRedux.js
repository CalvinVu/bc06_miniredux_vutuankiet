import { combineReducers } from "redux";
import { numberRedux } from "./numberRedux";

export const rootRedux = combineReducers({
  soLuong: numberRedux,
});
