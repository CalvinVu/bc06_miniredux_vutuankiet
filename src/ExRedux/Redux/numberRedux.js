let initialState = {
  number: 1,
};

export const numberRedux = (state = initialState, action) => {
  switch (action.type) {
    case "TANG_SO_LUONG": {
      state.number++;
      return { ...state };
    }
    case "GIAM_SO_LUONG": {
      return { ...state, number: state.number - action.payload };
    }
  }
  return state;
};
