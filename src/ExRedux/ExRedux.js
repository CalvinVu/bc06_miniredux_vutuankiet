import { connect } from "react-redux";
import React, { Component } from "react";

class ExRedux extends Component {
  render() {
    return (
      <div>
        <button
          className="btn btn-warning"
          onClick={() => {
            this.props.handleGiam(2);
          }}
        >
          -
        </button>
        <strong className="mx-5 h1">{this.props.curent}</strong>
        <button className="btn btn-success" onClick={this.props.handleTang}>
          +
        </button>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    curent: state.soLuong.number,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleTang: () => {
      let action = {
        type: "TANG_SO_LUONG",
      };
      dispatch(action);
    },
    handleGiam: (number) => {
      let action = {
        type: "GIAM_SO_LUONG",
        payload: number,
      };
      dispatch(action);
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ExRedux);
