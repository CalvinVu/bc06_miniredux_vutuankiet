import logo from "./logo.svg";
import "./App.css";
import ExRedux from "./ExRedux/ExRedux";

function App() {
  return (
    <div className="App">
      <ExRedux />
    </div>
  );
}

export default App;
